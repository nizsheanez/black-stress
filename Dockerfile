FROM alpine

RUN apk --no-cache add apache2-utils
RUN mkdir /src && mkdir /reports

ADD *.sh /src/
ADD nginx /etc/nginx/conf.d/

WORKDIR /src

VOLUME /reports
VOLUME /etc/nginx/conf.d/

CMD ./report.sh
