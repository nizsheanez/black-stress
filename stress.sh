#!/usr/bin/env sh
die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 4 ] || die "1st argument - amount of parallel requests, 2rd - total requests, 3rd - percentile, 4th - url"


# test
parallelRequests=$1
totalRequests=$2
percentile=$3
url=$4

ab -e report.csv -k -c $parallelRequests -n $totalRequests -s 60 $url > /dev/null

# format output, only read only 90 percentile
result=$(grep "${percentile}," report.csv | cut -d ',' -f 2 | cut -d '.' -f 1)
echo "$(date +'%Y-%m-%d %H:%M'),${result}"
