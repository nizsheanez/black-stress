#!/usr/bin/env sh

percentile=90

homeFile='/reports/home.csv'
salesFile='/reports/sales.csv'
productFile='/reports/product.csv'

ensure () {
    [ ! -e $1 ] && echo "Time, ${percentile} percentile in milliseconds" > $1
}

ensure $homeFile
ensure $salesFile
ensure $productFile

echo "HomePage\n"
./stress.sh 100 10000 $percentile 'http://blackstarshop.ru/' >> $homeFile
echo "CategoryPage\n"
./stress.sh 100 10000 $percentile 'http://blackstarshop.ru/catalog/sales/' >> $salesFile
echo "ProductPage\n"
./stress.sh 100 10000 $percentile 'http://blackstarshop.ru/catalog/51/p2444/?option_value_id=219' >> $productFile
echo "Done\n"